enum Role { ADMIN, READ_ONLY };     // enum

/*const person: {
    name: string;
    age: number;
    hobbies: string[];              // array
    role: [number, string];         // tuple
} = {*/
const person = {
    name: "Fede",
    age: 29,
    hobbies: ['Sports', 'Cooking'],
    role: [2, 'author'],
    other_role: Role.ADMIN,
};

person.role.push('admin');
person.role[0] = 10;

let favoriteActivities: string[];
favoriteActivities = ['Sports'];

console.log(person.name);

for (const hobby of person.hobbies) {
    console.log(hobby.toUpperCase());
}

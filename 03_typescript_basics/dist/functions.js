"use strict";
function add(n1, n2) {
    return n1 + n2;
}
function addAndHandle(n1, n2, cb) {
    const result = n1 + n2;
    cb(result);
}
function printResult2(num) {
    console.log('Result: ' + num);
}
printResult2(add(5, 23));
// ---
// ---
let combineValues;
combineValues = add;
// This presents an error
// combineValues = printResult;
console.log(combineValues(8, 8));
addAndHandle(10, 20, (result) => {
    console.log(result);
});
//# sourceMappingURL=functions.js.map
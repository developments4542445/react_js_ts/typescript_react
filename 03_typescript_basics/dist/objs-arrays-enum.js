"use strict";
var Role;
(function (Role) {
    Role[Role["ADMIN"] = 0] = "ADMIN";
    Role[Role["READ_ONLY"] = 1] = "READ_ONLY";
})(Role || (Role = {}));
; // enum
/*const person: {
    name: string;
    age: number;
    hobbies: string[];              // array
    role: [number, string];         // tuple
} = {*/
const person = {
    name: "Fede",
    age: 29,
    hobbies: ['Sports', 'Cooking'],
    role: [2, 'author'],
    other_role: Role.ADMIN,
};
person.role.push('admin');
person.role[0] = 10;
let favoriteActivities;
favoriteActivities = ['Sports'];
console.log(person.name);
for (const hobby of person.hobbies) {
    console.log(hobby.toUpperCase());
}
//# sourceMappingURL=objs-arrays-enum.js.map
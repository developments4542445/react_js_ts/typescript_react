### Commands:
To compile a TypeScript file
tsc <FILE.ts>

To avoid tsc <FILE.ts> every time that there is a change in code
tsc <FILE.ts> -w

npm init
npm install --save-dev lite-server
npm start

Creating tsconfig.json settings file for the entire project
tsc --init
Then run the next line to compile
tsc -w

Set in tsconfig.json:
- "sourceMap": true  => to enable debug
- "rootDir": "./src" => to include ts files
- "outDir": "./dist" => to include js files

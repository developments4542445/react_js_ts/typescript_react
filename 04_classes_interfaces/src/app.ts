// type AddFn = (a: number, b: number) => number;
interface AddFn {
    (a: number, b: number): number;
}

let add: AddFn;

add = (n1: number, n2: number) => {
    return n1+n2;
}

// ---
// ---
// ---

interface Named {
    readonly name?: string;
    outputName?: string;
}

interface Greetable extends Named {
    // 'readonly' is available in interfaces -> only set once during process
    // readonly name: string;
    greet(phrase: string): void;
}

class Person implements Greetable{
    // '?' sign indicates that this variable is optional
    name?: string; 
    age = 30; 

    constructor(name?: string) {
        // If name is not there, value is 'undefined'
        if (name) {
            this.name = name;
        }
    }

    greet(phrase: string) {
        if (this.name) {
            console.log(phrase + ' ' + this.name);
        } else {
            console.log("Hi without a name!");
        }
    }
}

let user1: Greetable;
let user2: Greetable;

user1 = new Person('Max');
user2 = new Person();

user1.greet('Hi there - I am' );
user2.greet('Hi there - I am' );

console.log(user1);

// 'abstract' generates a void method that will be used in children classes
// 'abstract' classes cannot be instantiated (in this case, Department)
abstract class Department {
    static fiscalYear = 2024;
    // name: string;                        // Default variables are public
    // private employees: string[] = [];    // Only accessible from this specific class
    protected employees: string[] = [];     // Accessible form this class and its children

    // We can init variables directly in the constructor. Example: name variable
    // If there is a 'readonly' variable, it WILL NOT be modified later
    constructor (protected readonly id: string, public name: string) {
        this.name = name;
        this.id = id;
    }

    // This is a method that does not require to be called from a class previously instanciated
    static createEmployee(name: string) {
        return {name: name};
    }

    abstract describe(this: Department): void

    addEmployee(employee: string) {
        // The next line is not allowed in readonly variables
        // this.id = "other";
        
        this.employees.push(employee);
    }

    printEmployeeInformation() {
        console.log(this.employees.length);
        console.log(this.employees);
    }
}

// Inheritance example
class ITDepartment extends Department {
    admins: string[];
    constructor(id: string, admins: string[]) {
        // Calling the constructor of the parent class
        super(id, 'IT');
        this.admins = admins;
    }

    describe() {
        console.log('IT department - ID: ' + this.id);
    }

}

// Inheritance example
class AccountingDepartment extends Department {
    private lastReport: string;
    private static instance: AccountingDepartment;

    // Getter
    get mostRecentReport() {
        if (this.lastReport) {
            return this.lastReport;
        }

        throw new Error('No report found.');
    }

    // Setter
    set mostRecentReport(value: string) {
        if (!value) {
            throw new Error('Please, pass a valid value.');
        }
        this.addReport(value);
    }

    private constructor(id: string, private reports: string[]) {
        super(id, 'Account');
        this.lastReport = reports[0];
    }

    static getInstance() {
        if (AccountingDepartment.instance) {
            return this.instance;
        }

        this.instance = new AccountingDepartment('d2', []);
        return this.instance;
    }

    describe() {
        console.log('Accounting department - ID: ' + this.id);
    }

    addEmployee(name: string) {
        if (name === 'Max') {
            return;
        }

        // Private variables are only accessible from the class that has been declared
        this.employees.push(name);
    }

    addReport(text:string) {
        this.reports.push(text);
        this.lastReport = text;
    }

    printReports() {
        console.log(this.reports);
    }
}

// Static method example
const employee1 = Department.createEmployee('Max');
console.log(Department.fiscalYear);
console.log(employee1);

const it = new ITDepartment('d1', ['Max']);

it.addEmployee("Max");
it.addEmployee("Manu");

// The next line is not allowed in private variables
// accounting.employees[2] = "Anna";

it.describe();
it.printEmployeeInformation();

console.log(it);

// Instanciate of a normal class
// const accounting = new AccountingDepartment('d2', []);

// Singletone implementation
const accounting = AccountingDepartment.getInstance();
console.log(accounting);

// Non valid setter example
// accounting.mostRecentReport = "";

// Non valid getter example
// console.log(accounting.mostRecentReport);

accounting.addReport('Something went wrong...');

accounting.addEmployee("Max");
accounting.addEmployee("Manu");

accounting.describe();
accounting.printReports();
accounting.printEmployeeInformation();
console.log("Hello world");

// Comparison between type and interface 

type Admin = {
    name: string;
    privileges: string[];
}

type Employee = {
    name: string;
    startDate: Date;
}

type ElevatedEmployee = Admin & Employee;

const e1: ElevatedEmployee = {
    name: "Max",
    privileges: ['create-server'],
    startDate: new Date()
}

// ----

interface Admin2 {
    name: string;
    privileges: string[];
}

interface Employee2 {
    name: string;
    startDate: Date;
}

interface ElevatedEmployee2 extends Employee2, Admin2 {};

const e2: ElevatedEmployee = {
    name: "Max",
    privileges: ['create-server'],
    startDate: new Date()
}

// Intersections with type
type Combinable = string | number;
type Numeric = number | boolean;

type Universal = Combinable | Numeric;

// ----------------------------------------------------------------
// ----------------------------------------------------------------

function add(a: Combinable, b: Combinable) {
    if (typeof a === 'string' || typeof b === 'string') {
        return a.toString() + b.toString();
    }

    return a+b;
}

type UnknownEmployee = Employee | Admin;

function printEmployeeInformation(emp: UnknownEmployee) {
    console.log("Name: " + emp.name);
    if ('privileges' in emp) {
        console.log("Privileges: " + emp.privileges);
    }

    if ('startDate' in emp) {
        console.log("Date: " + emp.startDate);
    }
}

printEmployeeInformation(e1);
console.log("---");
console.log("---");
printEmployeeInformation({name: "Manu", startDate: new Date()});

// ----------------------------------------------------------------
// ----------------------------------------------------------------

class Car {
    drive() {
        console.log("Driving...");
    }
}

class Truck {
    drive() {
        console.log("Driving a truck...");
    }

    loadCargo(amount: number) {
        console.log("Loading cargo... " + amount);

    }
}

type Vehicle = Car | Truck;

const v1 = new Car();
const v2 = new Truck();

function useVehicle(vehicle: Vehicle) {
    vehicle.drive();
    // instanceof is only present in interfaces
    if (vehicle instanceof Truck) {
        vehicle.loadCargo(1000);
    }
}

console.log("---");
console.log("---");
useVehicle(v1);
console.log("---");
console.log("---");
useVehicle(v2);

// ----------------------------------------------------------------
// ----------------------------------------------------------------

console.log("---");
console.log("---");

interface Bird {
    type: "bird";
    flyingSpeed: number;
}

interface Horse {
    type: "horse";
    runningSpeed: number;
}

type Animal = Bird | Horse;

function moveAnimal(animal: Animal) {
    let speed: number;
    switch(animal.type) {
        case 'bird':
            speed = animal.flyingSpeed;
            break;

        case 'horse':
            speed = animal.runningSpeed;
            break;
    }

    console.log("Moving at speed: " + speed);
}

moveAnimal({type: 'bird', flyingSpeed: 10});

// ----------------------------------------------------------------
// ----------------------------------------------------------------

console.log("---");
console.log("---");

const paragraph = document.getElementById("message-output");
// One way to cast
// const userInputElement = <HTMLInputElement> document.getElementById("user-input")!;
// Another way to cast
const userInputElement = <HTMLInputElement> document.getElementById("user-input")! as HTMLInputElement;

// Reminder -> The '!' sign at the end of the declaration indicates that that variable will never be null.

userInputElement.value = "Hi there";

// ----------------------------------------------------------------
// ----------------------------------------------------------------

console.log("---");
console.log("---");

interface ErrorContainer {
    // Without knowing the exact input name, I ask for a variable which name is taken as a string and its value is a string
    [prop: string]: string;
}

const errorBag: ErrorContainer = {
    email: 'Not a vaild email',
    username: "Must start with a capital character!"
};

// ----------------------------------------------------------------
// ----------------------------------------------------------------

console.log("---");
console.log("---");

// Overloading a function
function add2(a: number, b: number): number;
function add2(a: string, b: string): string;
function add2(a: Combinable, b: Combinable) {
    if (typeof a === 'string' || typeof b === 'string') {
        return a.toString() + b.toString();
    }

    return a+b;
}

const result = add2("Max", " Schwarz");
result.split(' ');
console.log(result);

// ----------------------------------------------------------------
// ----------------------------------------------------------------

console.log("---");
console.log("---");

// Let's imagine an object that does not contain job and we try to access that value
const fetchedUserData = {
    id: "u1",
    name: "Max",
    // job: {title: "CEO", description: "My own company"}
};

// If exists, the next line will be executed. If not, it will pass
// console.log(fetchedUserData?.job?.title);

// ----------------------------------------------------------------
// ----------------------------------------------------------------

console.log("---");
console.log("---");

const userInput = null;
// If userInput is null or undefined use its value. If not, use the other one
const storedData = userInput ?? 'DEFAULT';
console.log(storedData);

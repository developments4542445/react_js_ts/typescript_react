### Commands:
To compile a TypeScript file
tsc <FILE.ts>

To avoid tsc <FILE.ts> every time that there is a change in code
tsc <FILE.ts> -w

npm init
npm install --save-dev lite-server
npm start

Creating tsconfig.json settings file for the entire project
tsc --init
Then run the next line to compile
tsc -w

Installing third party libraries:
npm i --save lodash
npm install --save-dev @types/lodash
npm install class-transformer --save
npm install reflect-metadata --save
npm install class-validator --save

Set in tsconfig.json:
- "sourceMap": true  => to enable debug
- "rootDir": "./src" => to include ts files
- "outDir": "./dist" => to include js files

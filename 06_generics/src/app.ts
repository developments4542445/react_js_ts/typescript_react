console.log("Hello world");

/*
const names: Array<string> = ["Max", "Manuel"];        // Array <any> -> so I change it to a specific type
// names[0].split(' ');

const promise: Promise<string> = new Promise((resolve, reject) => {      // Promise<unknown> -> so I change it to a specific type
    setTimeout(() => {
        resolve("This is done!");
    }, 2000);
});

promise.then(data => {
    data.split(' ');
})
*/
// ------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------

function merge<T extends object, U extends object>(objA: T, objB: U) {
    return Object.assign(objA, objB);
}

console.log(merge({name: "Max"}, {age: 30}));

const mergedObj = merge({name: "Max"}, {age: 30});
// We cannot acces the age item
console.log(mergedObj.age);


// ------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------

console.log("---");
console.log("---");

function merge2<T extends object, U extends object>(objA: T, objB: U) {
    return Object.assign(objA, objB);
}

const mergedObj2 = merge({name: "Max"}, {age: 30});
console.log(mergedObj2);

// ------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------

console.log("---");
console.log("---");

// Including an interface to announce that my variable has a length property
interface Lengthy {
    length: number;
}

function countAndDescribe<T extends Lengthy>(element: T): [T, string] {
    let descriptionText = "Got no value";

    if (element.length === 1) {
        descriptionText = "Got 1 element.";
    } else if (element.length > 1) {
        descriptionText = "Got " + element.length + " elements.";
    }

    return [element, descriptionText];
}

console.log(countAndDescribe("Hi there!"));

// ------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------

console.log("---");
console.log("---");

// We want to check if var2 is inside of var1 AS A KEY
function extractAndConvert<T extends object, U extends keyof T>(obj: T, key: U) {
    return "Value: " + obj[key];
}

console.log(extractAndConvert({name: "Max"}, "name"));

// ------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------

console.log("---");
console.log("---");

class DataStorage<T> {
    private data: T[] = [];

    addItem(item: T) {
        this.data.push(item);
    }

    removeItem(item: T) {
        if (this.data.indexOf(item) === -1) {
            return;
        }
        this.data.splice(this.data.indexOf(item), 1);
    }

    getItems() {
        return [...this.data];
    }
}

const textStorage = new DataStorage<string>();
textStorage.addItem("Max");
textStorage.addItem("Manu");
textStorage.removeItem("Max");
console.log(textStorage.getItems());

const numberStorage = new DataStorage<number | string>();
numberStorage.addItem(5);
numberStorage.addItem("Manu");
numberStorage.removeItem("Manu");
console.log(numberStorage.getItems());

const objStorage = new DataStorage<object>();
const MaxObj = {name: "Max"};
objStorage.addItem(MaxObj);         // We need THE SAME OBJECT HERE... not the same data
objStorage.addItem({name: "Manu"});
// ...
objStorage.removeItem(MaxObj);
console.log(objStorage.getItems());

// ------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------

console.log("---");
console.log("---");

interface CourseGoal {
    title: string;
    description: string;
    completeUntil: Date;
}

// Pärtial tipe: an object that AT THE END will include all items included. Firstly, things are optional
function createCourseGoal (title: string, description: string, date: Date): CourseGoal {
    let courseGoal: Partial<CourseGoal> = {};
    courseGoal.title = title;
    courseGoal.description = description;
    courseGoal.completeUntil = date;

    return courseGoal as CourseGoal;
}

// Including a READONLY array -> does not allow push or pop
const names: Readonly<string[]> = ["Max", "Anna"];
// names.push("Manu");
// names.pop();
